FROM python:3-slim

COPY fake-project /tmp/fake-project

RUN pip install -U platformio \
    && pio platform install espressif8266 espressif32 atmelavr teensy
RUN pio run -d /tmp/fake-project\
    && pio run -d /tmp/fake-project --target clean \
    && rm -rf /tmp/fake-project
# RUN curl -LJO https://github.com/platformio/platformio-examples/archive/develop.zip \
#     && unzip platformio-examples-develop.zip \
#     && cd platformio-examples-develop\wiring-blink \
#     && pio run \
#     && pio run -e esp32doit-devkit-v1 \
#     && pio run --target clean
# RUN mkdir fake-project \
#     && cd fake-project \
#     && pio project init --board uno --board nodemcuv2 --board esp32doit-devkit-v1 \
#     && touch src\main.cpp \
#     && pio run \
#     && pio run --target clean \
#     && cd.. \
#     && rmdir -rf fake-project \
